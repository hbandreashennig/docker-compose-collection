#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

echo "Check APP_DIR"
APP_DIR=$(grep -r "APP_DIR=" .env | cut -d= -f2)
ls ${APP_DIR} > /dev/null 2>&1
APP_DIR_CHECK=${?}

if [[ ${APP_DIR_CHECK} -ne 0 ]]; then
    echo "APP_DIR not found: creating directory ..."
    mkdir -p ${APP_DIR}
fi

echo "Check DATA_DIR"
DATA_DIR=$(grep -r "DATA_DIR=" .env | cut -d= -f2)
ls ${DATA_DIR} > /dev/null 2>&1
DATA_DIR_CHECK=${?}

if [[ ${DATA_DIR_CHECK} -ne 0 ]]; then
    echo "DATA_DIR not found: creating directory ..."
    mkdir -p ${DATA_DIR}
fi

. ./bin/subscripts/services.sh

echo "Starting the following services: ${SERVICES}"

./bin/subscripts/docker-compose.sh up -d ${SERVICES}
cd - > /dev/null

printf "INFO: script ${0} runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"
